# HTTPD - веб-сервер

## Язык реализации
python 3.9

## Описание
Веб сервер, частично реализующий протокол HTTP.
В качестве решения для масштабирования используются воркеры, каждый из которых представляет отдельный тред


# Использование
### Запуск с конфигурацией из по умолчанию
```bash
$ python httpd.py
```

Параметры запуска:

-w --workers - количество воркеров, создаваемых веб сервером (по умолчанию 1)

-r --root - корневая директория для веб сервера (по умолчанию текущая директория)

-l --log - путь к файлу для записи логов (по умолчанию в stdout)

-i --ip ip - адрес, на котором будет запущен веб сервер (по умолчанию '0.0.0.0')

-p --port - порт, на котором будет запущен веб сервер (по умолчанию 8080)

-b --backlog - размер очереди для входящих соединений (по-умолчанию 100) 

### Запуск тестов
```bash
$ python httptest.py
```

### Результат нагрузочного тестирования
Нагрузочное тестирование выполнялось для следующей конфигурации:
python httpd.py -p 9090 -w 10 -b 150

ab -n 50000 -c 100 -r http://localhost:9090/httptest/dir2/

This is ApacheBench, Version 2.3 <$Revision: 1879490 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 5000 requests
Completed 10000 requests
Completed 15000 requests
Completed 20000 requests
Completed 25000 requests
Completed 30000 requests
Completed 35000 requests
Completed 40000 requests
Completed 45000 requests
Completed 50000 requests
Finished 50000 requests


Server Software:        httpd
Server Hostname:        localhost
Server Port:            9090

Document Path:          /httptest/dir2/
Document Length:        34 bytes

Concurrency Level:      100
Time taken for tests:   22.652 seconds
Complete requests:      50000
Failed requests:        0
Total transferred:      8449966 bytes
HTML transferred:       1699966 bytes
Requests per second:    2207.31 [#/sec] (mean)
Time per request:       45.304 [ms] (mean)
Time per request:       0.453 [ms] (mean, across all concurrent requests)
Transfer rate:          364.29 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       7
Processing:     0   45   7.8     44     132
Waiting:        0   44   7.7     43     103
Total:          0   45   7.8     44     132

Percentage of the requests served within a certain time (ms)
  50%     44
  66%     46
  75%     48
  80%     50
  90%     56
  95%     62
  98%     67
  99%     72
 100%    132 (longest request)