from enum import Enum
from optparse import OptionParser
import logging
from os.path import isfile, isdir, join, getsize, splitext
from os import getcwd
from queue import Queue, Empty
import threading
import socket
from email.utils import formatdate
import mimetypes
from urllib.parse import unquote


class RequestMethod(Enum):
    GET = 'GET'
    HEAD = 'HEAD'


class ResponseCode(Enum):
    OK = 200
    FORBIDDEN = 403
    NOT_FOUND = 404
    METHOD_NOT_ALLOWED = 405


MESSAGES_FOR_CODES_MAP = {
    ResponseCode.OK.value: 'OK',
    ResponseCode.FORBIDDEN.value: 'Forbidden',
    ResponseCode.NOT_FOUND.value: 'Not Found',
    ResponseCode.METHOD_NOT_ALLOWED.value: 'Method Not Allowed'
}

DEFAULT_INDEX = 'index.html'
DEFAULT_BACKLOG_SIZE = 100
DEFAULT_PORT = 8080
DEFAULT_WORKERS_COUNT = 1


class Request:
    method = None
    path = None
    path_full = None
    request_protocol = None

    def __init__(self, client_socket, client_address, root_directory: str):
        ip, port = client_address
        self.client_address = f'{ip}:{port}'
        self.client_socket = client_socket
        self.request_file = client_socket.makefile('rb')
        self.root_directory = root_directory

    def parse_request(self):
        max_line_size = 65537
        request_line = self.request_file.readline(max_line_size).decode('utf-8')
        try:
            method, path, protocol = request_line.strip().split(' ')
        except ValueError:
            logging.error(f'Unable to parse request {request_line}')
            return
        self.method = method.upper()
        self.path = unquote(path.split('?')[0]).replace('../', '')
        self.path_full = join(self.root_directory, self.path.lstrip('/'))
        self.request_protocol = protocol
        logging.info(f'New request received {self.client_address} {method} {path} {protocol}')


class Response:
    response_status = None
    response_headers = {}
    response_body = None
    allowed_methods = (RequestMethod.GET.value, RequestMethod.HEAD.value)

    def __init__(self, request: Request):
        self.request = request

    def process(self):
        self.response_status = ResponseCode.NOT_FOUND.value

        if self.request.method not in self.allowed_methods:
            self.response_status = ResponseCode.METHOD_NOT_ALLOWED.value
        elif isfile(self.request.path_full):
            self.response_status = ResponseCode.OK.value
            self.response_body = self.request.path_full
        elif isdir(self.request.path_full):
            index_file = join(self.request.path_full, DEFAULT_INDEX)
            if isfile(index_file):
                self.response_status = ResponseCode.OK.value
                self.response_body = index_file

    def set_response_headers(self):
        self.response_headers = {
            'Date': formatdate(timeval=None, localtime=False, usegmt=True),
            'Server': 'httpd',
            'Connection': 'close',
            'Content-Type': 'text/html; charset="utf8"',
            'Content-Length': 0
        }
        if self.response_body:
            self.response_headers['Content-Length'] = getsize(self.response_body)
            name, extension = splitext(self.response_body)
            content_type = mimetypes.types_map.get(extension)
            if content_type:
                self.response_headers['Content-Type'] = content_type

    def send(self):
        self.set_response_headers()
        headers = f'HTTP/1.1 {self.response_status} {MESSAGES_FOR_CODES_MAP.get(self.response_status)}\r\n'
        for header_name, header_value in self.response_headers.items():
            headers += f'{header_name}: {header_value}\r\n'
        headers += '\r\n'
        self.request.client_socket.sendall(headers.encode())

        if self.response_body and self.request.method != RequestMethod.HEAD.value:
            with open(self.response_body, 'rb') as f:
                for line in f:
                    try:
                        self.request.client_socket.send(line)
                    except BrokenPipeError:
                        logging.info(f'Got broken pipe while processing request {self.request.client_address}')
        self.close()

    def close(self):
        self.request.request_file.close()
        self.request.client_socket.close()


class Worker(threading.Thread):
    def __init__(self, requests_queue: Queue, name: str, root_directory: str):
        super().__init__()
        self.requests_queue = requests_queue
        self.name = name
        self.root_directory = root_directory
        logging.info(f'Created worker {name}')

    def run(self):
        while True:
            try:
                client_socket, client_address = self.requests_queue.get_nowait()
            except Empty:
                continue
            request = Request(
                client_socket=client_socket, client_address=client_address, root_directory=self.root_directory
            )
            request.parse_request()
            response = Response(request=request)
            response.process()
            response.send()
            self.requests_queue.task_done()


class SimpleHTTPServer:
    def __init__(self, ip: str, port: int, workers_count: int, root_directory: str, backlog_size: int):
        self.server_socket = None
        self.ip = ip
        self.port = port
        self.workers_count = workers_count
        self.root_directory = root_directory
        self.backlog_size = backlog_size
        self.workers_pool = []
        self.requests_queue = Queue()

    def create_workers_pool(self):
        for i in range(self.workers_count):
            worker = Worker(self.requests_queue, f'worker{i + 1}', self.root_directory)
            worker.start()
            self.workers_pool.append(worker)

    def serve_forever(self):
        self.create_workers_pool()
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(self.backlog_size)
        while True:
            client_socket, client_address = self.server_socket.accept()
            self.requests_queue.put_nowait((client_socket, client_address))


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-w", "--workers", action="store", type=int, default=DEFAULT_WORKERS_COUNT)
    op.add_option("-r", "--root", action="store", default=getcwd())
    op.add_option("-l", "--log", action="store", default=None)
    op.add_option("-i", "--ip", action="store", default='0.0.0.0')
    op.add_option("-p", "--port", action="store", type=int, default=DEFAULT_PORT)
    op.add_option("-b", "--backlog", action="store", type=int, default=DEFAULT_BACKLOG_SIZE)
    (opts, args) = op.parse_args()
    logging.basicConfig(
        filename=opts.log, level=logging.INFO, format="[%(asctime)s] %(levelname).1s %(message)s", datefmt="%Y.%m.%d %H:%M:%S",
    )

    server = SimpleHTTPServer(
        ip=opts.ip, port=opts.port, workers_count=opts.workers, root_directory=opts.root, backlog_size=opts.backlog
    )
    server.serve_forever()
